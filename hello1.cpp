///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07c - My First Cat - EE 205 - Spr 2022
///
/// @file hello1.cpp
/// @version 1.0
///
/// 1st Hello World program
///
/// @author Adrian Peng <apeng2@hawaii.edu>
/// @date   25_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#include <iostream>

using namespace std;

int main() {

   cout << "Hello World!" << endl;


}
